package com.example.androidskripsi.utils;

public class Constant {
    public static final String URL = "http://192.168.8.101/";
    public static final String HOME = URL + "api/";
    public static final String LOGIN = HOME + "login";
    public static final String LOGOUT = HOME + "logout";

    //presence
    public static final String PRESENCE = HOME + "presence";
    public static final String ADD_PRESENCE = PRESENCE + "/create";
}
