package com.example.androidskripsi.fragment.presence;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.androidskripsi.MainActivity;
import com.example.androidskripsi.MySingleton;
import com.example.androidskripsi.R;
import com.example.androidskripsi.activity.presence.AddPresenceActivity;
import com.example.androidskripsi.adapter.PresenceAdapter;
import com.example.androidskripsi.models.Presence;
import com.example.androidskripsi.models.User;
import com.example.androidskripsi.utils.Constant;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class PresenceFragment extends Fragment {
    private View view;
    private MaterialToolbar toolbar;
    private FloatingActionButton fab;
    private SharedPreferences sharedPreferences;
    private SwipeRefreshLayout refreshLayout;
    public static RecyclerView recyclerViewPresence;
    ;
    public static ArrayList<Presence> arrayListPresence;
    private PresenceAdapter presenceAdapter;
    Uri imgUri;
    private static final int GALLERY_ADD_POST = 1001;
    private static final int PERMISSION_CODE = 1000;

    public PresenceFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_presence, container, false);
        init();
        return view;
    }

    private void init() {
        sharedPreferences = getContext().getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
        recyclerViewPresence = view.findViewById(R.id.recyclerPresence);
        recyclerViewPresence.setHasFixedSize(true);
        recyclerViewPresence.setLayoutManager(new LinearLayoutManager(getContext()));
        refreshLayout = view.findViewById(R.id.swipePresence);
        toolbar = view.findViewById(R.id.toolbarPresence);
        fab = view.findViewById(R.id.fab);
        setHasOptionsMenu(true);
        ((MainActivity) getContext()).setSupportActionBar(toolbar);

        getPresence();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPresence();
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED || getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
                        String[] permission = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
                        requestPermissions(permission, PERMISSION_CODE);
                    } else {
                        openCamera();
                    }
                } else {
                    openCamera();
                }
            }
        });
    }

    private void openCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        imgUri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        startActivityForResult(cameraIntent, GALLERY_ADD_POST);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Intent i = new Intent(((MainActivity)getContext()), AddPresenceActivity.class);
            i.setData(imgUri);
            startActivity(i);
        }
    }

    private void getPresence() {
        arrayListPresence = new ArrayList<>();
        refreshLayout.setRefreshing(true);
        StringRequest request = new StringRequest(Request.Method.GET, Constant.PRESENCE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getBoolean("success")){
                        JSONArray array = new JSONArray(object.getString("presence"));
                        for (int i =0; i < array.length(); i++) {
                            JSONObject presenceObject = array.getJSONObject(i);
                            JSONObject userObject = presenceObject.getJSONObject("user");

                            User user = new User();
                            user.setId(userObject.getInt("id"));
                            user.setName(userObject.getString("name"));
                            user.setPhoto(userObject.getString("photo"));

                            Presence presence = new Presence();
                            presence.setId(presenceObject.getInt("id"));
                            presence.setUser(user);
                            presence.setDate(presenceObject.getString("created_at").toString());
                            presence.setAddress(presenceObject.getString("address"));
                            presence.setPhoto(presenceObject.getString("photo"));

                            arrayListPresence.add(presence);
                        }
                        presenceAdapter = new PresenceAdapter(getContext(), arrayListPresence);
                        recyclerViewPresence.setAdapter(presenceAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                refreshLayout.setRefreshing(false);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                refreshLayout.setRefreshing(false);
            }
        }) {
            //provide token in header
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String token = sharedPreferences.getString("token", "");
                HashMap<String, String> map = new HashMap<>();
                map.put("Authorization", "Bearer " + token);
                return map;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestQueue queue = MySingleton.getInstance(getContext().getApplicationContext()).getRequestQueue();
//        RequestQueue queue = Volley.newRequestQueue(getContext());
        MySingleton.getInstance(getContext()).addToRequestQueue(request);
//        queue.add(request);
    }

}