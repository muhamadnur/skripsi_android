package com.example.androidskripsi.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidskripsi.Interface.ItemClickListener;
import com.example.androidskripsi.R;
import com.example.androidskripsi.models.Presence;
import com.example.androidskripsi.utils.Constant;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class PresenceAdapter extends RecyclerView.Adapter<PresenceAdapter.PresenceHolder> {

    private Context context;
    private ArrayList<Presence> list;
    private ArrayList<Presence> listAll;
    private SharedPreferences preferences;

    public PresenceAdapter(Context context, ArrayList<Presence> list) {
        this.context = context;
        this.list = list;
        this.listAll = new ArrayList<>(list);
        preferences = context.getApplicationContext().getSharedPreferences("user", Context.MODE_PRIVATE);
    }

    @NonNull
    @Override
    public PresenceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_presences, parent, false);
        return new PresenceHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PresenceHolder holder, int position) {
        final Presence presence = list.get(position);
        Picasso.get().load(Constant.URL + "storage/profile/" + presence.getUser().getPhoto()).into(holder.imgProfile);
//        Picasso.get().load(Constant.URL + "storage/presence/" + presence.getPhoto()).into(holder.imgProfile);
        holder.txtName.setText(presence.getUser().getName());
        holder.txtDate.setText(presence.getDate());
        holder.txtAddress.setText(presence.getAddress());

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(View v, int pos) {
                final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(context);
                View view = LayoutInflater.from(v.getRootView().getContext()).inflate(R.layout.layout_show_picture_presence, null);
                ImageView imagePresence;
                ImageButton imageClose;
                imagePresence = view.findViewById(R.id.imagePresence);
                imageClose = view.findViewById(R.id.imageClose);
                Picasso.get().load(Constant.URL + "storage/presence/" + presence.getPhoto()).into(imagePresence);

                imageClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bottomSheetDialog.cancel();
                    }
                });
                bottomSheetDialog.setContentView(view);
                bottomSheetDialog.setCancelable(true);
                bottomSheetDialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class PresenceHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtName, txtDate, txtAddress;
        private CircleImageView imgProfile;
        private Button btnDetail;
        ItemClickListener itemClickListener;

        public PresenceHolder(@NonNull View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtAddress = itemView.findViewById(R.id.txtAddress);
            imgProfile = itemView.findViewById(R.id.imgProfile);
            btnDetail = itemView.findViewById(R.id.btnDetail);

            btnDetail.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener ic) {
            this.itemClickListener = ic;
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(v, getLayoutPosition());
        }
    }
}
